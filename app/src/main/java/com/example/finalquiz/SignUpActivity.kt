package com.example.finalquiz

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_result.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.emailEditText
import kotlinx.android.synthetic.main.activity_sign_up.passwordEditText
import kotlinx.android.synthetic.main.activity_sign_up.progressBar


class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var reference: DatabaseReference



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("users")
    }


    private fun init() {
        auth = Firebase.auth
        signUpButton1.setOnClickListener {
            signup()

        }
    }


    private fun signup() {
        val email = emailEditText.text.toString().trim()
        val username = usernameTextEdit.text.toString().trim()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()


        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty() && username.isNotEmpty()) {
            if (password == repeatPassword) {
                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    progressBar.visibility = View.VISIBLE
                    signUpButton1.isClickable = false
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            progressBar.visibility = View.GONE
                            signUpButton1.isClickable = true
                            if (task.isSuccessful) {
                                d("signUp", "createUserWithEmail:success")
                                val user = auth.currentUser!!

                                val model = User()
                                model.username = username
                                model.email = email
                                //var model=User(username, email)
                                model.uid = auth.currentUser!!.uid

                                reference.child("users").child(user.uid).setValue(model)




                                val intent = Intent(this, QuizActivity::class.java)
                                startActivity(intent)
                            } else {
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(
                                    baseContext, "მოხდა შეცდომა:"+" "+task.exception?.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                } else {
                    Toast.makeText(this, "შეიყვანეთ სწორი იმეილი", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,"პაროლები არ ემთხვევა",Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(this,"შეავსეთ ყველა ველი",Toast.LENGTH_SHORT).show()
        }
    }
}