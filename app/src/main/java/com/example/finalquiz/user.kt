package com.example.finalquiz

data class User(
    var uid: String? = ",",
    var username: String? = "",
    var email: String? = "",
    var score: Int? = 0
)


data class Question (
    val id: Int,
    val question: String,
    val image: Int,
    val optionOne: String,
    val optionTwo: String,
    val optionThree: String,
    val optionFour: String,
    val correctAnswer: Int
)









