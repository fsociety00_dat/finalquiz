package com.example.finalquiz

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.activity_result.*
import java.util.HashMap

class QuizActivity : AppCompatActivity(), View.OnClickListener {

    private var mCurrentPosition: Int = 1
    private var mQuestionsList: ArrayList<Question>? = null
    private var mSelectedOptionPosition: Int = 0
    private var mCorrectAnswers: Int = 0



    //////////////////////////
    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var reference: DatabaseReference
    //////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        ////////////////////////
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("users")
        ////////////////////////
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_quiz)


        mQuestionsList = Constants.getQuestions()


        setQuestion()

        option_one.setOnClickListener(this)
        option_two.setOnClickListener(this)
        option_three.setOnClickListener(this)
        option_four.setOnClickListener(this)

        submit_btn.setOnClickListener(this)

    }




    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.option_one -> {

                selectedOptionView(option_one, 1)
            }

            R.id.option_two -> {

                selectedOptionView(option_two, 2)
            }

            R.id.option_three -> {

                selectedOptionView(option_three, 3)
            }

            R.id.option_four -> {

                selectedOptionView(option_four, 4)
            }



            R.id.submit_btn -> {

                if (mSelectedOptionPosition == 0) {

                    if (submit_btn.text == "შემდეგი კითხვა"){mCurrentPosition++}

                    if (submit_btn.text == "პასუხი"){ Toast.makeText(this@QuizActivity, "აირჩიეთ პასუხი", Toast.LENGTH_SHORT).show() }

                    when {

                        mCurrentPosition <= mQuestionsList!!.size -> {

                            setQuestion()
                        }
                        else -> {
                            val intent = Intent(this, ResultActivity::class.java)
                            intent.putExtra(Constants.CORRECT_ANSWERS, mCorrectAnswers)
                            intent.putExtra(Constants.TOTAL_QUESTIONS, mQuestionsList!!.size)
                            startActivity(intent)

                        }
                    }
                } else {
                    val question = mQuestionsList?.get(mCurrentPosition - 1)



                        if (question!!.correctAnswer != mSelectedOptionPosition) {
                            answerView(mSelectedOptionPosition, R.drawable.wrong_option_border_bg)
                        } else {
                            mCorrectAnswers++

                        }

                        answerView(question.correctAnswer, R.drawable.correct_option_border_bg)

                    if (mCurrentPosition == mQuestionsList!!.size) {
                        submit_btn.text = "პასუხი"
                        if (mSelectedOptionPosition != 0){
                                submit_btn.text = "დასრულება"
                            option_one.isClickable = false
                            option_two.isClickable = false
                            option_three.isClickable = false
                            option_four.isClickable = false
                            submit_btn.setOnClickListener {
                                val intent = Intent(this, ResultActivity::class.java)
                                intent.putExtra(Constants.CORRECT_ANSWERS, mCorrectAnswers)
                                intent.putExtra(Constants.TOTAL_QUESTIONS, mQuestionsList!!.size)
                                val score = mCorrectAnswers
                                val user = auth.currentUser!!

                                reference.child("users").child(user.uid).
                                addValueEventListener(object: ValueEventListener{
                                    override fun onCancelled(error: DatabaseError) {
                                        TODO("Not yet implemented")
                                    }

                                    override fun onDataChange(snapshot: DataSnapshot) {
                                        val user = snapshot.getValue(User::class.java)

                                        if (user != null)
                                        {

                                            val record:Int = user.score!!.toInt()
                                            if (score > record){
                                                reference.child("users").child(user.uid.toString()).child("score").setValue(score)
                                            }

                                        }
                                    }
                                })



                                startActivity(intent)
                            }
                        }

                    } else {
                        submit_btn.text = "შემდეგი კითხვა"
                        option_one.isClickable = false
                        option_two.isClickable = false
                        option_three.isClickable = false
                        option_four.isClickable = false
                    }

                    mSelectedOptionPosition = 0
                }
            }
        }
    }



    private fun setQuestion() {
        option_one.isClickable = true
        option_two.isClickable = true
        option_three.isClickable = true
        option_four.isClickable = true

        val question =
            mQuestionsList!!.get(mCurrentPosition - 1)

        defaultOptionsView()


        if (mCurrentPosition == mQuestionsList!!.size){
            submit_btn.text = "პასუხი"
            if (mSelectedOptionPosition != 0){
                    submit_btn.text = "დასრულება"
                option_one.isClickable = false
                option_two.isClickable = false
                option_three.isClickable = false
                option_four.isClickable = false
                submit_btn.setOnClickListener {
                    val intent = Intent(this, ResultActivity::class.java)
                    intent.putExtra(Constants.CORRECT_ANSWERS, mCorrectAnswers)
                    intent.putExtra(Constants.TOTAL_QUESTIONS, mQuestionsList!!.size)
                    startActivity(intent)
                }

            }



        }else{
            submit_btn.text = "პასუხი"
        }

        lineProgressBar.progress = mCurrentPosition
        progressText.text = "$mCurrentPosition" + "/" + lineProgressBar.max

        tv_question.text = question.question
        //questionImage.setImageResource(question.image)
        option_one.text = question.optionOne
        option_two.text = question.optionTwo
        option_three.text = question.optionThree
        option_four.text = question.optionFour
    }







    private fun selectedOptionView(tv: TextView, selectedOptionNum: Int) {

        defaultOptionsView()

        mSelectedOptionPosition = selectedOptionNum

        tv.setTextColor(
            Color.parseColor("#1a001c")
        )
        tv.setTypeface(tv.typeface, Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(
            this@QuizActivity,
            R.drawable.selected_option_border_bg
        )
    }

    private fun defaultOptionsView() {

        val options = ArrayList<TextView>()
        options.add(0, option_one)
        options.add(1, option_two)
        options.add(2, option_three)
        options.add(3, option_four)

        for (option in options) {
            option.setTextColor(Color.parseColor("#38003c"))
            option.typeface = Typeface.DEFAULT
            option.background = ContextCompat.getDrawable(
                this@QuizActivity,
                R.drawable.default_option_border_bg
            )
        }
    }

    private fun answerView(answer: Int, drawableView: Int){
        when(answer){
            1 ->{
                option_one.background = ContextCompat.getDrawable(
                    this, drawableView
                )
            }
            2 ->{
                option_two.background = ContextCompat.getDrawable(
                    this, drawableView
                )
            }
            3 ->{
                option_three.background = ContextCompat.getDrawable(
                    this, drawableView
                )
            }
            4 ->{
                option_four.background = ContextCompat.getDrawable(
                    this, drawableView
                )
            }
        }
    }

}


