package com.example.finalquiz

object Constants {

        const val TOTAL_QUESTIONS: String = "კითხვების რაოდენობა"
    const val CORRECT_ANSWERS: String = "სწორი პასუხები"

    fun getQuestions(): ArrayList<Question>{
        val questionsList = ArrayList<Question>()

        val que1 = Question(1,
            "რომელმა ჰოკაგემ ჩაბეჭდა ცხრაკუდა მელა ნარუტოში?",
            R.drawable.ic_naruto,
            "მეორემ",
            "მეოთხემ",
            "მესამემ",
            "პირველმა",
            2
        )
        questionsList.add(que1)

        val que2 = Question(2,
            "რომელ პერსონაჟს შეუძლია მხოლოდ ტაიჯუცუს გამოყენება?",
            R.drawable.ic_launcher_background,
            "ნარუტოს",
            "საკურას",
            "როკ ლის",
            "გაარას",
            3
        )
        questionsList.add(que2)


        val que3 = Question(3,
            "ნარუტო პირველად აჩენს ცხრაკუდას ჩაკრას ____ ბრძოლისას.",
            R.drawable.ic_launcher_background,
            "ჰაკუსთან",
            "სასკესთან",
            "ოროჩმარუსთან",
            "ნეჯისთან",
            1
        )
        questionsList.add(que3)

        val que4 = Question(4,
            "სასკეს მიზანია გაძლიერდეს, იმის გამო რომ მოკლას?",
            R.drawable.ic_launcher_background,
            "ოროჩიმარუ",
            "კაკაში ჰატაკე",
            "იტაჩი უჩიჰა",
            "მესამე ჰოკაგე",
            3
        )
        questionsList.add(que4)


        val que5 = Question(5,
            "რომელი ლეგენდარული სანინი ხდება მეხუთე ჰოკაგე?",
            R.drawable.ic_launcher_background,
            "ცუნადე",
            "ჯირაია",
            "ოროჩიმარუ",
            "არცერთი",
            1
        )
        questionsList.add(que5)

        val que6 = Question(6,
            "დ-რანკის მისიებს უმეტესად აძლევენ?",
            R.drawable.ic_launcher_background,
            "ჯონინებს",
            "ჰოკაგეებს",
            "ჩუნინებს",
            "გენინებს",
            4
        )
        questionsList.add(que6)

        val que7 = Question(7,
            "რომელ ჯგუფში არიან ნარუტო, სასკე და საკურა",
            R.drawable.ic_launcher_background,
            "მეხუთე",
            "მეშვიდე",
            "პირველი",
            "მეათე",
            2
        )
        questionsList.add(que7)

        val que8 = Question(8,
            "რა სახის ჯუცუა შარინგანი?",
            R.drawable.ic_launcher_background,
            "ნინჯუცუ",
            "დოჯუცუ",
            "გენჯუცუ",
            "ტაიჯუცუ",
            2
        )
        questionsList.add(que8)

        val que9 = Question(9,
            "რა არის 106სმ?",
            R.drawable.ic_launcher_background,
            "მისტიკური ხმალი",
            "საიდუმლო გრაგნილი",
            "კიბას ძაღლი",
            "ცუნადეს მკერდის გარშემოწერილობა",
            4
        )
        questionsList.add(que9)

        val que10 = Question(10,
            "რა არის მინატოს ზედმეტსახელი?",
            R.drawable.ic_launcher_background,
            "ელვა",
            "ფოთლების სოფლის ელვა",
            "კონოჰას ყვითელი ელვა",
            "ელვისებრი ნინძა",
            3
        )
        questionsList.add(que10)

        val que11 = Question(11,
            "რა შესაძლებლობა აქვს კაკაშის/ობიტოს შარინგანს?",
            R.drawable.ic_launcher_background,
            "კამუი",
            "კოტოამაცუკამი",
            "ამატერასუ",
            "იზანაგი",
            1
        )
        questionsList.add(que11)

        val que12 = Question(12,
            "ვინ იყვნენ ინდრასა და აშურას პირველი რეინკარნაცია?",
            R.drawable.ic_launcher_background,
            "სასკე და იტაჩი",
            "კაკაში და გაი",
            "ნარუტო და სასკე",
            "მადარა და ჰაშირამა",
            4
        )
        questionsList.add(que12)

        val que13 = Question(13,
            "ვის აკოცა ნარუტომ პირველად?",
            R.drawable.ic_launcher_background,
            "საკურას",
            "ჰინატას",
            "სასკეს",
            "ინოს",
            3
        )
        questionsList.add(que13)

        val que14 = Question(14,
            "ვინ შეძლო სუსანოს ერთი თვალით გამოძახება?",
            R.drawable.ic_launcher_background,
            "მადარამ",
            "ობიტომ",
            "შისუიმ",
            "კაკაშიმ",
            3
        )
        questionsList.add(que14)

        val que15 = Question(15,
            "რა ჰქვია კაკაშის საიდუმლო ტექნიკას?",
            R.drawable.ic_launcher_background,
            "სიკვდილის 1000 წელი",
            "რაიკირი",
            "კამუი",
            "ჩიდორი",
            1
        )
        questionsList.add(que15)



        return questionsList


    }

}