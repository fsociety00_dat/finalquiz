package com.example.finalquiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {


    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var reference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        database = FirebaseDatabase.getInstance()
        reference = database.getReference("users")



        val totalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS, 0)
        val correctAnswers = intent.getIntExtra(Constants.CORRECT_ANSWERS, 0)
        scoreText.text = "თქვენ აიღეთ $correctAnswers/$totalQuestions ქულა"
        if(0<=correctAnswers && correctAnswers<=5){
            resImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_bad_res))
            resText.text = "გირჩევ ნარუტოს უყურო"
        }else if (6<=correctAnswers && correctAnswers<=10){
            resImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_ave_res))
            resText.text = "ახლიდან ყურება ცუდი არ იქნებოდა"
        }else{
            resImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_naruto))
            resText.text = "ყოჩაღ! ნამდვილი ფანი ხარ"
        }
        auth = Firebase.auth
        val user = auth.currentUser
        reference.child("users").child(user!!.uid).
        addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)

                if (user != null)
                {
                    val uname:String = user.username.toString()
                    val score:String = user.score.toString()
                    record_tv.text = uname+": პირადი რეკორდი: "+score



                }
            }
        })





        restartButton.setOnClickListener {
            intent = Intent(this, QuizActivity::class.java)
            startActivity(intent)

        }



    }

}




